#ifndef PATHS_HPP
#define PATHS_HPP

#include <string>

namespace paths
{

extern const std::string gfx_path;

extern const std::string fonts_path;
extern const std::string tiles_path;
extern const std::string images_path;

extern const std::string logo_img_path;
extern const std::string skull_img_path;

extern const std::string save_path;

} // paths

#endif // PATHS_HPP
