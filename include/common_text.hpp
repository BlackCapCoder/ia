#ifndef COMMON_MESSAGES_HPP
#define COMMON_MESSAGES_HPP

#include <string>

namespace common_text
{

extern const std::string screen_exit_hint;
extern const std::string scrollable_info_screen_hint;
extern const std::string next_page_up_hint;
extern const std::string next_page_down_hint;
extern const std::string cancel_hint;
extern const std::string confirm_hint;
extern const std::string any_key_hint;
extern const std::string yes_or_no_hint;
extern const std::string disarm_no_trap;
extern const std::string mon_prevent_cmd;
extern const std::string fire_prevent_cmd;
extern const std::string mon_disappear;

} // common_text

#endif // COMMON_MESSAGES_HPP
